window.onload = function () {
  /* api youtube */
  var tag = document.createElement("script");
  tag.src = "https://www.youtube.com/iframe_api";
  var firstScriptTag = document.getElementsByTagName("script")[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  /* function genarl datalayer */
  const createrEventDataLayer = (idform, category, action, label) => {
    //console.log("form", jQuery(idform).valid());
    if (jQuery(idform).valid()) {
      dataLayer.push({
        event: "trackEvent",
        eventCategory: category,
        eventAction: action,
        eventLabel: label,
        eventValue: "_VALUE_",
      });
    }
  };

  /* Datalayer Contact us */
  jQuery("#webform-client-form-16 .webform-submit").click(function (e) {
    createrEventDataLayer(
      "#webform-client-form-16",
      "Contact us",
      "Clic",
      "Subbmit"
    );
  });

  /* Datalayer slick title */
  jQuery(".view-id-brand_types .views-row .views-field-title").click(function (
    e
  ) {
    dataLayer.push({
      event: "trackEvent",
      eventCategory: "Our Brand",
      eventAction: jQuery(this).find("a").text(),
      eventLabel: "Click",
      eventValue: "_VALUE_",
    });
  });
  /* set value datalayer video */
  setTimeout(() => {
    if (jQuery("body").hasClass("front")) {
      let Idcurrent = jQuery(".slick-current iframe")[0].attributes["id"].value;
      let title = jQuery(".slick-current .views-field-title .field-content p").text();
      videoload(Idcurrent, title);
      /* click next */
      jQuery(".view-slider-video .slick-next, .view-slider-video .slick-prev").click(function (e) {
        let Idcurrent = jQuery(".slick-current iframe")[0].attributes["id"].value;
        let title = jQuery(".slick-current .views-field-title p").text();
        videoload(Idcurrent, title);
      });
    }
  }, 1000);
};

function videoload(idFrame, title) {
  var player;
  function onYouTubeIframeAPIReady() {
    player = new YT.Player(idFrame, {
      events: {
        onStateChange: onPlayerStateChange,
      },
    });
  }
  var done = false;
  function onPlayerStateChange(event) {
    if (event.data == YT.PlayerState.PLAYING && !done) {
      dataLayer.push({
        event: "trackEvent",
        eventCategory: title,
        eventAction: "Click",
        eventLabel: "Play",
        eventValue: "_VALUE_",
      });

      done = true;
    }
  }
  onYouTubeIframeAPIReady();
}
