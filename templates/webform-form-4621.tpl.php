
<div class="sab-data-request">
  <div class="fill-form-elements">
    <div class="col-xs-12 col-md-6"><?php print drupal_render($form['submitted']['header_text']); ?></div>

      <div class="sab-webform-elements">

        <div class="col-xs-12 col-md-6"><?php print drupal_render($form['submitted']['select_request_type']); ?></div>

        <div class="display-desktop-inline">
          <div class="col-xs-12 col-md-6"><?php print drupal_render($form['submitted']['first_name']); ?></div>
          <div class="col-xs-12 col-md-6"><?php print drupal_render($form['submitted']['surname']); ?></div>
        </div>

        <div class="display-desktop-inline">
          <div class="col-xs-12 col-md-6"><?php print drupal_render($form['submitted']['email']); ?></div>
          <div class="col-xs-12 col-md-6"><?php print drupal_render($form['submitted']['country_of_residence']); ?></div>
        </div>

        <div class="col-xs-12 col-md-6"><?php print drupal_render($form['submitted']['message']); ?></div>
        <div class="submit-and-captcha">
        <?php
          print drupal_render_children($form); 
          print drupal_render($form['actions']['submit']); 
        ?>
        </div>
      </div>
  </div>
</div>
<style>
.sab-data-request .fill-form-elements .sab-webform-elements {
    display: inline-grid;
}

.sab-data-request .fill-form-elements .sab-webform-elements .form-item {
    margin: 10px 0;
}

.sab-data-request .fill-form-elements .col-md-6 {
    width:  100%;
}

.sab-data-request .fill-form-elements .col-md-6 a {
    color: #BAA769;
}

.sab-data-request .fill-form-elements .sab-webform-elements .webform-submit {
    width: 100px;
    background-color: #BAA769;
    border-style: none;
    height: 38px;
}

.sab-data-request .fill-form-elements .sab-webform-elements .webform-component-select {
    position: relative;
}

.sab-data-request .fill-form-elements .sab-webform-elements .submit-and-captcha .rc-anchor-dark.rc-anchor-normal {
    background-color: #f9f9f9;
    color: initial;
}

div#rc-anchor-container {
    background: red;
    color: initial;
}

.sab-data-request .fill-form-elements .sab-webform-elements .form-control {
    min-height: 40px;
}

.sab-data-request .fill-form-elements .sab-webform-elements .webform-component-select:after {
    content: "";
    position: absolute;
    right: 10px;
    top: 40px;
    width: 0; 
    height: 0; 
    border-left: 10px solid transparent;
    border-right: 10px solid transparent;
    border-top: 10px solid #333;
    border-radius: 4px;
}

.sab-data-request .fill-form-elements .sab-webform-elements .submit-and-captcha {
    text-align: end;
    text-align: right;
}

.submitted-data-request .button-primary {
    background: #BAA769;
    border: none;
    margin: 5px 0;
    width: 250px;
}

.submitted-data-request .button-primary.dark{
    background: #000000;
}

.submitted-data-request {
    text-align:center;
}

.submitted-data-request h1 {
    font-weight: bold;
}

@media only screen and (max-width: 768px) {

    .sab-data-request .fill-form-elements .sab-webform-elements .webform-submit {
        margin-right: 15px;
    }
    .sab-data-request .fill-form-elements .sab-webform-elements .submit-and-captcha {
        margin-left: 15px;
    }
}

@media only screen and (min-width: 768px) {
    .sab-data-request .fill-form-elements .sab-webform-elements .webform-submit {
        margin-left: 260px;
    }

    .submitted-data-request .submit-buttons .button {
        width: 50%;
        display:inline;
    }

    .submit-and-captcha {
        text-align: end;
        text-align: right;
    }

    .sab-data-request .fill-form-elements .sab-webform-elements {
        width: 70%;
        margin: 0 15%;
    }

    .sab-data-request .fill-form-elements .sab-webform-elements .display-desktop-inline .col-md-6 {
        width:  50%;
    }

    .sab-data-request .fill-form-elements .sab-webform-elements .submit-and-captcha {
        display: inline-flex;
        margin-left: 15px;
    }

    span.submitted {
        visibility: hidden;
    }
}
</style>