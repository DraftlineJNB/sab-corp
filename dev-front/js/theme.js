jQuery(document).ready(function ($) {
    let isScreenMobile = $(window).width() < 992;

    // -- General -- //
    const $GeneralScope = {
        // Constructor
        init: function () {
            this.menuScripts();
            this.navigationSnippet();
            this.setBackgroundImage();
            this.searchDesktop();
            this.sliderTabs();
            // this.jpaginate();
        },

        //jpaginate
        jpaginate: function () {
            $(".jpaginate-3").paginate({
                items_per_page: 3
            });
            $(".jpaginate-6").paginate({
                items_per_page: 6
            });
            $(".jpaginate-9").paginate({
                items_per_page: 9
            });

            $(".wrapper-date .one").on("click", function () {
                // console.log("debug .....");
                $("form.ctools-auto-submit-full-form").change();
                $("form.ctools-auto-submit-full-form")
                    .add(".ctools-auto-submit")
                    .filter("form, select, input:not(:text, :submit)")
                    .once("ctools-auto-submit")
                    .change(function (e) {
                        // don't trigger on text change for full-form
                        if ($(e.target).is(":not(:text, :submit, .ctools-auto-submit-exclude)")) {
                            triggerSubmit.call(e.target.form);
                        }
                    });
            });
        },

        // Menu scripts
        menuScripts: function () {
            let ButtonTrigger = $(".navbar-header .navbar-toggle"),
                LogoMenu = $(".navbar-header .logo"),
                MenuWrapper = $(".navbar-header .navbar-collapse"),
                NavHeader = $(".navbar-header");

            if (ButtonTrigger) {
                $(document).on("click", ".navbar-toggle", function () {
                    LogoMenu.toggleClass("open-menu");
                    ButtonTrigger.toggleClass("collapsed open-menu");
                    NavHeader.toggleClass("open-menu");
                    $("body").toggleClass("fixed");
                });
            }
        },

        navigationSnippet: function () {
            $('a[href*="#"]')
                // Remove links that don't actually link to anything
                .not('[href="#"]')
                .not('[href="#0"]')
                .click(function (event) {
                    // On-page links
                    if (
                        location.pathname.replace(/^\//, "") ==
                        this.pathname.replace(/^\//, "") &&
                        location.hostname == this.hostname
                    ) {
                        // Figure out element to scroll to
                        let target = $(this.hash);

                        target = target.length ?
                            target :
                            $("[name=" + this.hash.slice(1) + "]");
                        // Does a scroll target exist?
                        if (target.length) {
                            // Only prevent default if animation is actually gonna happen
                            event.preventDefault();
                            $("html, body").animate({
                                scrollTop: target.offset().top
                            }, 1000, function () {
                                // Callback after animation
                                // Must change focus!
                                let $target = $(target);
                                $target.focus();

                                if ($(window).width() <= 768) {
                                    menu_wrapper.slideUp(300);
                                }

                                if ($target.is(":focus")) {
                                    // Checking if the target was focused
                                    return false;
                                } else {
                                    $target.attr("tabindex", "-1"); // Adding tabindex for elements not focusable
                                    $target.focus(); // Set focus again
                                }
                            });
                        }
                    }
                });
        },

        setBackgroundImage: function () {
            // let Body = $("body"),
            //     DataBackgroundDesktop = Body.data("bg-desktop"),
            //     HeaderMain = $("header.navbar-default");
            var Body = $("body"),
                DataBackgroundDesktop = Body.data("bg-desktop"),
                DataBackgroundMobile = Body.data("bg-mobile"),
                HeaderMain = $("header.navbar-default");

            // HeaderMain.attr("style", 'background-image:url("' + DataBackgroundDesktop + '")');
            if ($(window).width() <= 991) {
                if (DataBackgroundMobile != "") {
                    HeaderMain.attr("style", 'background-image:url("' + DataBackgroundMobile + '")');
                } else {
                    HeaderMain.attr("style", 'background-image:url("' + DataBackgroundDesktop + '")');
                }
            } else {
                HeaderMain.attr("style", 'background-image:url("' + DataBackgroundDesktop + '")');
            }
        },

        searchDesktop: function () {
            let btnSearch = "#navbar-collapse > nav > ul > li.last.leaf > a",
                $blockSearch = $(".block-search");

            $(document).on("click", btnSearch, function (e) {
                e.preventDefault();

                $blockSearch.toggleClass("open-search");
            });
        },

        sliderTabs: function () {
            let $sliderTabs = $(".container-tab");

            if ($sliderTabs.length) {
                // Set first item
                if (isScreenMobile) {
                    // Tabs About Us
                    if ($("body").hasClass("sabc-ourvision")) {
                        $(".container-tab .tab-2").insertBefore('.container-tab .tab-1')
                    } else if ($("body").hasClass("sabc-leadership")) {
                        $(".container-tab .tab-3").insertBefore('.container-tab .tab-1')
                    } else if ($("body").hasClass("sabc-contrib")) {
                        $(".container-tab .tab-4").insertBefore('.container-tab .tab-1')
                    }

                    // Tabs Our Community
                    if ($("body").hasClass("sabc-transformation")) {
                        $(".container-tab .tab-2").insertBefore('.container-tab .tab-1')
                    } else if ($("body").hasClass("sabc-sustainability")) {
                        $(".container-tab .tab-3").insertBefore('.container-tab .tab-1')
                    } else if ($("body").hasClass("sabc-smartdrinking")) {
                        $(".container-tab .tab-4").insertBefore('.container-tab .tab-1')
                    }

                    // Tabs Our Stories
                    if ($("body").hasClass("sabc-125years")) {
                        $(".container-tab .tab-2").insertBefore('.container-tab .tab-1')
                    } else if ($("body").hasClass("documents")) {
                        $(".container-tab .tab-3").insertBefore('.container-tab .tab-1')
                    } else if ($("body").hasClass("custom-gallery")) {
                        $(".container-tab .tab-4").insertBefore('.container-tab .tab-1')
                    }

                    // Tabs Type of Documents
                    if ($("body").hasClass("images-logos")) {
                        $(".container-tab .tab-2").insertBefore('.container-tab .tab-1')
                    } else if ($("body").hasClass("approved-video")) {
                        $(".container-tab .tab-3").insertBefore('.container-tab .tab-1')
                    }

                    // Tabs Contact Us
                    if ($("body").hasClass(" ")) {
                        $(".container-tab .tab-2").insertBefore('.container-tab .tab-1')
                    } else if ($("body").hasClass(" ")) {
                        $(".container-tab .tab-3").insertBefore('.container-tab .tab-1')
                    }

                    // Tabs Legal
                    if ($("body").hasClass("sabc-ourdream")) {
                        $(".container-tab .tab-2").insertBefore('.container-tab .tab-1')
                    } else if ($("body").hasClass("sabc-cookiepolicy")) {
                        $(".container-tab .tab-2").insertBefore('.container-tab .tab-1')
                    } else if ($("body").hasClass("sabc-privacypolicy")) {
                        $(".container-tab .tab-3").insertBefore('.container-tab .tab-1')
                    }

                    // Tabs Contact Us
                    if ($("body").hasClass("sabc-siteslocation")) {
                        $(".container-tab .tab-2").insertBefore('.container-tab .tab-1')
                    }
                }

                $sliderTabs.slick({
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    responsive: [{
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1
                        },
                    }, ],
                });
            }
        }
    };

    // -- Agegate -- //
    const $AgegateScope = {
        // Constructor
        init: function () {
            this.ageScripts();
            this.moveSectionFooterIntoFooter();
        },

        // scripts for Agegate
        ageScripts: function () {
            // Dom manipulation
            let ListCountries = $(".form-item-list-of-countries"),
                Checklist = $(".form-item-remember-me"),
                FbValidate = $(".age_checker_facebook_validate"),
                RememberMe = $(".ab-inbev-remember-me"),
                RememberLabel = Checklist.find("label"),
                RememberMeStr = RememberMe.find("strong"),
                FooterContent = $(".ab-inbev-footer");

            if (ListCountries) {
                ListCountries.insertAfter("#age_checker_error_message");
            }

            // if(Checklist) {
            //  Checklist.append(RememberMe)
            //  RememberLabel.append(RememberMeStr)
            // }

            // if(FbValidate){
            //  FbValidate.insertAfter('#edit-submit')
            //  FbValidate.append('<span class="fbTrigger">Sign in with <b>facebook</b></span>')
            // }

            if (FooterContent) {
                $('.agegate-container-footer').append(FooterContent)
            }
        },

        moveSectionFooterIntoFooter: function () {
            let SectionMenu = $("#block-menu-menu-footer-menu");

            if (SectionMenu) {
                SectionMenu.insertAfter(".ab-inbev-footer .ab-inbev-footer-content-1 .ab-inbev-footer-aware-logo");
                $(".ab-inbev-footer-content-1 .menu-wrapper").hide();
            }
        }
    };

    // -- Home -- //
    const $HomeScope = {
        // Constructor
        init: function () {
            // Instance functions
            this.sliderVideos();
            this.maxTextNews();
            this.heroSliders();
        },

        heroSliders: function () {
            $('.view-banners > .view-content').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                draggable: false,
                asNavFor: '.view-mini-banners > .view-content',
            });
            $('.view-mini-banners .view-content').slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                asNavFor: '.view-banners > .view-content',
                dots: false,
                arrows: false,
                centerMode: false,
                focusOnSelect: true,
                autoplay: true,
                autoplaySpeed: 3000,
                responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        autoplay: true,
                        autoplaySpeed: 3000,
                        dots: false,
                        arrows: false,
                    }
                }]
            });
            jQuery('.view-mini-banners .view-content .field-content .views-field-name a').click(function (e) {
                e.preventDefault()
            })
        },

        maxTextNews: function () {
            let textTitle = $('#block-views-news-block .views-row .views-field-title a'),
                textParagraph = $('#block-views-news-block .views-row .views-field-body p')

            textTitle.each(function () {
                $(this).text($(this).text().substring(0, 68) + '...')
            })

            textParagraph.each(function () {
                $(this).text($(this).text().substring(0, 250) + '...')
            })
        },

        sliderVideos: function () {
            let $sliderVideos = $(".view-slider-video .slider-component");

            $sliderVideos.find('.views-field-title').append('<span class="titles"></span>')

            $sliderVideos.on('init afterChange', function (event, slick, currentSlide, nextSlide) {
                let actualSlide = $(".view-slider-video .slick-current"),
                    prevTitle = actualSlide.prev().find('.views-field-title .field-content > p').text(),
                    nextTitle = actualSlide.next().find('.views-field-title .field-content > p').text()

                actualSlide.find('.views-field-title .titles').empty().html('<p>' + prevTitle + '</p><p>' + nextTitle + '</p>')
            });

            $sliderVideos.slick({
                infinite: true
            });



            // let listVideo = document.querySelectorAll(
            //  ".view-id-slider_video .slick-track .views-row"
            // );

            // for (let index = 0; index < listVideo.length; index++) {
            //  const element = listVideo[index];
            //  if (element.nextSibling != null) {
            //      let textNext = element.nextSibling.firstElementChild.innerText;
            //      let p = document.createElement("p");
            //      p.innerText = textNext;
            //      element.children[0].children[0].appendChild(p);
            //  }
            // }

            $sliderVideos.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                $('.slick-current iframe').attr('src', $('.slick-current iframe').attr('src'));
            });
        }
    };

    // -- News --//
    const $NewsScope = {
        init: function () {
            this.filter();
            this.temporalPaginator();
        },

        filter: function () {
            let listItem = document.querySelectorAll(
                ".view-id-related_articles .view-content .views-field-field-news-date .date-display-single"
            );
            let listYear = document.querySelectorAll(".wrapper-date .year");
            let valueYear = "0";
            let NumberItemsPaginate = document.querySelector(".wrapper-date")
                .attributes["data-number-item"].value;
            var arraylist = [];

            //Array save data of start-list
            arraylist = listItem;
            //Event click
            for (let index = 0; index < listYear.length; index++) {
                const element = listYear[index];
                element.addEventListener("click", function (e) {
                    let dataYear = e.target.attributes["data-year"].value;
                    Dofilter(dataYear, arraylist);
                });
            }

            //Function Do filter year and start-list
            const Dofilter = (year, list) => {
                let count = 0;
                let countNotFound = 0;

                //console.log("list request .. ", list);
                for (let index = 0; index < list.length; index++) {
                    const element = list[index];
                    let row = element.closest(".views-row");
                    let viewContent = document.querySelector(
                        ".view-related-articles .view-content ul"
                    );
                    let para = document.createElement("p");

                    if (element.textContent != year && year != "0") {
                        row.remove();
                        countNotFound++;
                        //Not result
                        if (count == 0 && countNotFound == list.length) {
                            let notFoundNot = document.querySelector(".not-found");
                            let control = document.querySelector(".pagination__controls");
                            if (control != null) {
                                control.remove();
                            }
                            //only one not repeat text
                            if (notFoundNot == null) {
                                para.innerText = "Not found";
                                para.classList.add("not-found");
                                viewContent.appendChild(para);
                            }
                        }
                    } else {
                        let notFoundNot = document.querySelector(".not-found");
                        viewContent.appendChild(row);
                        count++;
                        if (notFoundNot != null) {
                            notFoundNot.remove();
                        }
                        if (count > NumberItemsPaginate) {
                            viewContent.classList.add("jpaginate-filter");
                        } else {
                            let control = document.querySelectorAll(".pagination__controls");
                            viewContent.classList.remove("jpaginate-filter");
                            viewContent.classList.add("less-3");
                            if (control != null) {
                                for (let index = 0; index < control.length; index++) {
                                    const element = control[index];
                                    element.remove();
                                }
                            }
                        }
                    }
                }
                let children = document.querySelectorAll(".less-3 li");
                if (children != null) {
                    for (let index = 0; index < children.length; index++) {
                        const element = children[index];
                        element.style.display = "block";
                    }
                }
                $(".jpaginate-filter").paginate({
                    items_per_page: NumberItemsPaginate,
                    prev_next: false
                });
            };
            Dofilter(valueYear, arraylist);
        },

        temporalPaginator: function () {
            const slider = document.querySelector('.sabc-news .pagination');
            let isDown = false;
            let startX;
            let scrollLeft;

            slider.addEventListener('mousedown', (e) => {
                isDown = true;
                slider.classList.add('active');
                startX = e.pageX - slider.offsetLeft;
                scrollLeft = slider.scrollLeft;
            });

            slider.addEventListener('mouseleave', () => {
                isDown = false;
                slider.classList.remove('active');
            });

            slider.addEventListener('mouseup', () => {
                isDown = false;
                slider.classList.remove('active');
            });

            slider.addEventListener('mousemove', (e) => {
                if (!isDown) return;
                e.preventDefault();
                const x = e.pageX - slider.offsetLeft;
                const walk = (x - startX) * 3; //scroll-fast
                slider.scrollLeft = scrollLeft - walk;
            });

            $(document).on('click', '.sabc-news .pagination li', function () {
                window.scrollTo(0, 500)
            })
        }
    };

    // -- Video Approved --//
    const $VideoScope = {
        init: function () {
            this.video();
        },

        video: function () {
            $("button, input[type='button'], .close").on("click", function (event) {
                let ci = $(this).attr("cdata");
                $(
                    ".media-youtube-" + ci + " .media-youtube-player"
                )[0].contentWindow.postMessage(
                    '{"event":"command","func":"' + "stopVideo" + '","args":""}',
                    "*"
                );
            });
            $(".file-video-youtube").on("click", function (e) {
                $(this)
                    .parents(".views-field-field-video-youtube")
                    .siblings(".views-field-nothing")
                    .find(".sabc-cgallery-showvideo")
                    .click();
            });
            $(".sabc-cgallery-showvideo").on("click", function (event) {
                event.preventDefault();
                let racmodal = $(this).attr("cdata");
                $("#" + racmodal).modal({
                    backdrop: "static",
                    keyboard: false
                });
                $("#" + racmodal).modal("show");
            });
        }
    };

    // -- Brands --//
    const $BrandsScope = {
        init: function () {
            this.slickBrands()
        },

        slickBrands: function () {
            var mainSlider = ".page-brands #block-system-main > div > div.view-content > div > ul",
                listSlider = ".page-brands #block-system-main > div > div.attachment.attachment-after > div > div > div > ul"

            $(mainSlider).slick({
                adaptiveHeight: true,
                infinite: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                asNavFor: listSlider
            });

            $(listSlider).slick({
                infinite: false,
                arrows: true,
                slidesToShow: 5,
                slidesToScroll: 1,
                asNavFor: mainSlider,
                focusOnSelect: true,
                responsive: [{
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 4
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 2
                        }
                    }
                ]
            })
        }
    };

    // -- Temporal News --//
    const $TemporalNewsScope = {
        init: function () {
            this.generalScript();
        },

        generalScript: function () {
            const createListToSelect = () => {
                let $formYears = $("#views-exposed-form-test-news-page"),
                    $listYears = $("#edit-field-news-date-value-value-year"),
                    $newContainerYear = $("#news-filter-years")
                // arrayYears = []

                // $formYears.attr("method", "post")
                $newContainerYear.empty()

                $listYears.find("option").each(function (i, year) {
                    //if (year.value != "" && i != $listYears.find("option").length - 1)
                    if (year.value != "") {
                        let classActive = "";
                        if (year.selected) {
                            classActive = "active-year"
                        }
                        // arrayYears.push(year.value)
                        //console.log(year);
                        $newContainerYear.append('<div class="year ' + classActive + '" data-value="' + year.value + '">' + year.value + '</div>')
                    }
                })

                $newContainerYear.removeClass('slick-initialized slick-slider').slick({
                    slidesToShow: 5,
                    slidesToScroll: 5
                })

                $(document).on('click', '#news-filter-years .year', function (e) {
                    //$(this).addClass("active-year").siblings().removeClass("active-year");
                    let $el = $(e.currentTarget)
                    // $indexSlick = $el.data('slick-index')

                    $listYears.find("option[value='" + $el.data('value') + "']").attr("selected", "selected")
                    $formYears.find('.form-submit').click()
                    // $formYears.submit();

                    $(document).ajaxComplete(function () {
                        createListToSelect()

                        $newContainerYear.slick({
                            slidesToShow: 5,
                            slidesToScroll: 5
                        })
                    })
                })

                // let select = document.querySelectorAll(".sabcnews .date-year")[0].childNodes[0];

                // /* Create list */
                // for (let index = 0; index < select.length; index++) {
                //  const textYear = select[index].value;

                //  if(document.getElementById("news-filter-years").childElementCount <= select.length){
                //      let node = document.createElement("li");
                //      let textnode = document.createTextNode(textYear);

                //      node.appendChild(textnode);
                //      document.getElementById("news-filter-years").appendChild(node);
                //  }
                // }

                // let list = document.querySelectorAll("#news-filter-years li");

                // for (let index = 0; index < list.length; index++){
                //  const element = list[index];

                //  element.addEventListener("click", function (e){
                //      for (let index = 0; index < select.length; index++){
                //          const element = select[index];

                //          if (element.value == e.target.textContent) {
                //              element.setAttribute("selected", "selected");

                //              let formSubmit = document.querySelector("#edit-submit-test-news");

                //              formSubmit.click();

                //              $(document).ajaxComplete(function() {
                //                  createListToSelect();
                //              });
                //          }
                //      }
                //  });
                // }
            };

            createListToSelect();
        }
    };

    const $highlightSearched = {
        init: function () {
            this.highlightSearched();
        },

        highlightSearched: function () {
            var term = jQuery('#edit-name').val().trim().toLowerCase();
            if (term.length > 0) {
                var source = jQuery('.accordion .card').html();
                var words = source.split(' ');
                var output = '';
                jQuery.each(words, function (idx, word) {
                    if (term === word.toLowerCase()) {
                        output += '<span class="searched-red">' + word + '</span> ';
                    } else {
                        output += word + ' ';
                    }
                });

                jQuery('.accordion .card').html(output);
            }
        }
    }

    const $sliderSmartDrinkink = {
        init: function () {
            this.sliderSmartDrinkink();
        },
        sliderSmartDrinkink: function () {
            let sliderSD = jQuery('.view-news.view-display-id-block_1 .view-content');
            let sliderNews = jQuery('.view-news.view-display-id-block_news_harm_reduction .view-content');
            let sliderMar = jQuery('.view-sabc.view-display-id-block_responsible_marketing .view-content');
            let sliderDri = jQuery('.view-sabc.view-display-id-block_responsible_driving .view-content');
            let sliderCom = jQuery('.view-sabc.view-display-id-block_responsible_communities .view-content');
            let sliderT = jQuery('.view-drinking-brands.view-id-drinking_brands .view-content');
            let mediaQuery = $(window).width() > 992;
            if (mediaQuery == true) {
                sliderSD.slick({
                    arrows: true,
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 1
                });
            }
            sliderT.slick({
                arrows: true,
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 1,
                responsive: [{
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }]
            });
            sliderNews.slick({
                arrows: true,
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 1
            });
            sliderMar.slick({
                arrows: true,
                infinite: true,
                slidesToShow: 2,
                slidesToScroll: 1,
                responsive: [{
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        variableWidth: true
                    }
                }]
            });
            sliderDri.slick({
                arrows: true,
                infinite: true,
                slidesToShow: 2,
                slidesToScroll: 1,
                responsive: [{
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        variableWidth: true
                    }
                }]
            });
            sliderCom.slick({
                arrows: true,
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1
            });
        }
    }

    // ----------------------------
    // TRIGGERS
    // ----------------------------

    // Trigger
    $GeneralScope.init();

    // Agegate
    if ($("body").hasClass("page-agegate")) {
        $AgegateScope.init();
    }

    // Home Scripts
    if ($("body").hasClass("front")) {
        $HomeScope.init();
    }

    // News Scripts
    if ($("body").hasClass("sabc-news")) {
        $NewsScope.init();
    }

    // Temporal News Scripts
    if ($("body").hasClass("page-news")) {
        $TemporalNewsScope.init();
    }

    // Video Scripts
    if ($("body").hasClass("approved-video")) {
        $VideoScope.init();
    }

    // Brands Scripts
    if ($("body").hasClass("page-brands")) {
        $BrandsScope.init()
    }

    // Documents Scripts
    if ($("body").hasClass("documents")) {
        $highlightSearched.init()
    }

    // Documents Scripts
    if ($("body").hasClass("page-smark-drinking") || 
        $("body").hasClass("sabc-comunitites") || 
        $("body").hasClass("sabc-marketing") ||
        $("body").hasClass("sabc-driving") ||
        $("body").hasClass("sabc-retailing")) {
        $sliderSmartDrinkink.init();
    }

    jQuery(document).ajaxComplete(function() {
        let sliderMar = jQuery('.view-sabc.view-display-id-block_responsible_marketing .view-content');
        let sliderDri = jQuery('.view-sabc.view-display-id-block_responsible_driving .view-content');
        sliderMar.slick({
            arrows: true,
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 1,
            responsive: [{
                breakpoint: 991,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    variableWidth: true
                }
            }]
        });
        sliderDri.slick({
            arrows: true,
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 1,
            responsive: [{
                breakpoint: 991,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    variableWidth: true
                }
            }]
        });
      });
 

    $(document).on('click', '.item-title-smart-slide', function () {
        $(".item-title-smart-slide").removeClass("activebrand");
        $(this).addClass("activebrand");
        localStorage.setItem('size', $(this).data("size"));
        $(".img-brand-smart-drinks").html($(this).find(".imgbrandsslider").html());
        $(".inputsize").val($(this).data("size") + " ml")
        $("#block-block-116 .block-title").html($(this).find(".item-title-brand").html());
        $(".inputunits").val("")
    });

    if ($(window).width() <= 600) {
        $(document).on('click', '.slick-next , slick-prev', function () {
            localStorage.setItem('size', $("#block-views-drinking-brands-block .slick-list .slick-active .item-title-smart-slide").data("size"));
            $(".inputsize").val($("#block-views-drinking-brands-block .slick-list .slick-active .item-title-smart-slide").data("size") + " ml")
            $("#block-block-116 .block-title").html($("#block-views-drinking-brands-block .slick-active .item-title-brand").html());
            $(".img-brand-smart-drinks").html($("#block-views-drinking-brands-block .slick-active .imgbrandsslider").html());
            $(".inputunits").val("")
        });
    }
    $(document).on('click', '.item-title-smart-slide', function () {
        $(".item-title-smart-slide").removeClass("activebrand");
        $(this).addClass("activebrand");
        localStorage.setItem('size', $(this).data("size"));
        $(".img-brand-smart-drinks").html($(this).find(".imgbrandsslider").html());
        $(".inputsize").val($(this).data("size") + " ml")
        $("#block-block-116 .block-title").html($(this).find(".item-title-brand").html());
        $(".inputunits").val("")
    });



    $(document).on('click', '.btn-smart-drinks', function () {
        if (localStorage.getItem("size") === null) {
            $("#form-smart-drinks legend").html("Please select one brand")
        } else {
            var size = localStorage.getItem('size');
            var inputweek = $(".inputweek").val();
            if (size != "" && inputweek != "") {
                var cal = parseInt(size) + parseInt(inputweek) / 1000
                $(".inputunits").val(cal)
            } else {
                $("#form-smart-drinks legend").html("All fields are required")
            }
        }
    });

});